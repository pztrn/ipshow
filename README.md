# IPSHOW

Simple and small utility to show local IP addresses, e.g. for remote support units.

# Installation

```
go get -u -v go.dev.pztrn.name/ipshow
```

should be enough.

# Dependencies

* Zenity - for showing IP addresses.